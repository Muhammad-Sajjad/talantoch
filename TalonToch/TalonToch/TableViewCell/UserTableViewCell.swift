//
//  UserTableViewCell.swift
//  TalonToch
//
//  Created by Sajjad Malik on 26.02.22.
//

import UIKit

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var lblEmail: UILabel!
    static let identifier = "UserTableViewCell"
    var clickHandler: ((Int) -> Void)?
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    func watchForClickHandler(completion: @escaping (Int) -> Void) {
        self.clickHandler = completion
    }
    @IBAction func editBtnTapped(_ sender: UIButton) {
        guard let completion = self.clickHandler else {return}
        completion(1)
    }
    
    @IBAction func deleteBtnTaped(_ sender: UIButton) {
        guard let completion = self.clickHandler else {return}
        completion(0)
    }
    
}
