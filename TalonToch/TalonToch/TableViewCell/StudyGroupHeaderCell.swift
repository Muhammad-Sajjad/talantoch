//
//  StudyGroupHeaderCell.swift
//  TalonTouch
//
//  Created by syed zia on 31/10/2021.
//

import UIKit

class StudyGroupHeaderCell: UITableViewCell {

    static let identifier = "StudyGroupHeaderCell"
    @IBOutlet weak var titleLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
