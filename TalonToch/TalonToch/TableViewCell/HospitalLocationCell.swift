//
//  HospitalLocationCell.swift
//  TalonTouch
//
//  Created by syed zia on 01/11/2021.
//

import UIKit

class HospitalLocationCell: UITableViewCell {
    
    
    static let identifier = "HospitalLocationCell"
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var switchButton: UISwitch!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
