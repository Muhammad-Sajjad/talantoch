//
//  StudyGroupCell.swift
//  TalonTouch
//
//  Created by syed zia on 31/10/2021.
//

import UIKit

class StudyGroupCell: UITableViewCell {
    
    static let identifier = "StudyGroupCell"
    
    @IBOutlet weak var lblPatinetName: UILabel!
    @IBOutlet weak var lblModility: UILabel!
    @IBOutlet weak var lblStudyDate: UILabel!
    @IBOutlet weak var lblAccessionNo: UILabel!
    @IBOutlet weak var lblPatientId: UILabel!
    @IBOutlet weak var lblDiscription: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
