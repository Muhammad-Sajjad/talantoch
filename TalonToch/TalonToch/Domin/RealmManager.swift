//
//  RealmManager.swift
//  TalonToch
//
//  Created by Sajjad Malik on 11.02.22.
//

import Foundation
import RealmSwift


class RealmManager {
    
    // MARK: - Shared Variables
    static let shared = RealmManager()
    
    // MARK: - Variables
    
    lazy var realm = try! Realm()
    
    func deleteUsers() {
        let data = realm.objects(User.self)
        let users = data.toArray(ofType: User.self)
        for user in users.enumerated() {
            try? realm.safeWrite {
                realm.delete(user.element)
            }
        }
    }
    
    func deleteHospitalData() {
        let data = realm.objects(HospitalData.self)
        let users = data.toArray(ofType: HospitalData.self)
        for user in users.enumerated() {
            try? realm.safeWrite {
                realm.delete(user.element)
            }
        }
    }
}

extension Results {
    func toArray<Element>(ofType: Element.Type) -> [Element] {
        
        var array = [Element]()
        for i in 0 ..< count {
            if let result = self[i] as? Element {
                array.append(result)
            }
        }
        
        return array
    }
}
