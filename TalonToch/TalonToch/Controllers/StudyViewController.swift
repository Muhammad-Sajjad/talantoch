//
//  StudyViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 04.02.22.
//

import UIKit
import SkyFloatingLabelTextField

class StudyViewController: UIViewController {
    
    @IBOutlet weak var studtTableView: UITableView!
    @IBOutlet weak var pathalogyTableView: UITableView!
    
    @IBOutlet weak var pthalogyTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var studyTableViewHeight: NSLayoutConstraint!
    @IBOutlet weak var dropdownView: UIView!
    @IBOutlet weak var dropdownLable: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var advanceSearchView: UIView!
    
    @IBOutlet var patientNameField: SkyFloatingLabelTextField!
    @IBOutlet var patientIDField: SkyFloatingLabelTextField!
    @IBOutlet var accessionField: SkyFloatingLabelTextField!
    @IBOutlet var studyDateField: SkyFloatingLabelTextField!
    @IBOutlet var datePicker: UIDatePicker!
    @IBOutlet var modalitiesField: SkyFloatingLabelTextField!
    
    var isStudyOpen: Bool = false
    var isPathalogyOpen: Bool = false
    
    var studies = [Study]()
    
    var brestStudy = [Study]()
    var thyroidStudy = [Study]()
    let dropDown = DropDown()

    var dropdwonItems:[String]{
        return ["All Studies","Study 1", "Study 2", "Study 3"]
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        studtTableView.confirmDelegate(delegate: self, dataSource: self)
        studtTableView.register(StudyGroupCell.identifier)
        pathalogyTableView.confirmDelegate(delegate: self, dataSource: self)
        pathalogyTableView.register(StudyGroupCell.identifier)
        // Do any additional setup after loading the view.
        
        datePicker.tintColor  = .blue
        advanceSearchView.isHidden = true
        configure()
        loadStudy()
      

    }
    
    // MARK: - Custom Function
    func loadStudy() {
        if let fileLocation = Bundle.main.url(forResource: "studyData", withExtension: "json") {
            // do catch in case of an error
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([Study].self, from: data)
                
                self.studies = dataFromJson
                studtTableView.reloadData()
                getdata()
                //addUser()
            } catch {
                print(error)
            }
        }
    }
    
    func getdata() {
        for study in studies {
            if (study.mainDicomTags?.studyDescription?.contains("THYROID") == true) {
                thyroidStudy.append(study)
            } else {
                brestStudy.append(study)
            }
        }
        studtTableView.reloadData()
        pathalogyTableView.reloadData()
    }
    
    func configure(){
        
        dropDown.anchorView = dropdownView
        dropDown.dataSource = dropdwonItems
        dropDown.direction  = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.dropdownLable.text = item
        }
        
        
    }
    
    // MARK: - Action
    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        presentedViewController?.dismiss(animated: true, completion: nil)
        
    }
    @IBAction func crossBtnTapped(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.advanceSearchView.isHidden = true
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    
    @IBAction func homeBtnTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func dropDownTapped(_ sender: UIButton) {
        dropDown.show()
    }
    
    @IBAction func searchBtnTapped(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.advanceSearchView.isHidden = true
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    @IBAction func advanceSearchTapped(_ sender: UIButton) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.advanceSearchView.isHidden = false
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    @IBAction func studyHeaderTapped(_ sender: UIButton) {
        isStudyOpen.toggle()
        if isStudyOpen {
            setStudyHeight(height: 0)
        } else {
            setStudyHeight(height: 250)
        }
    }
    
    @IBAction func pathalogyHeaderTapped(_ sender: UIButton) {
        isPathalogyOpen.toggle()
        if isPathalogyOpen {
            setPthalogyHeight(height: 0)
        } else {
            setPthalogyHeight(height: 250)
        }
        
    }
    
    func setPthalogyHeight(height: CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.pthalogyTableViewHeight.constant = height
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setStudyHeight(height: CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.studyTableViewHeight.constant = height
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
}


// MARK: - View Extensions
extension StudyViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableView == studtTableView {
            return studies.count
        } else if tableView == pathalogyTableView {
            return thyroidStudy.count
        }
        return 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: StudyGroupCell.identifier, for: indexPath) as! StudyGroupCell
        
        if tableView == studtTableView {
            cell.lblPatinetName.text = studies[indexPath.row].patientMainDicomTags?.patientName
            cell.lblPatientId.text = studies[indexPath.row].patientMainDicomTags?.patientID
            cell.lblAccessionNo.text = studies[indexPath.row].mainDicomTags?.accessionNumber
            cell.lblStudyDate.text = studies[indexPath.row].mainDicomTags?.studyDate
            cell.lblModility.text = studies[indexPath.row].type
            cell.lblDiscription.text = studies[indexPath.row].mainDicomTags?.studyDescription
            
        }
        if tableView == pathalogyTableView {
            cell.lblPatinetName.text = thyroidStudy[indexPath.row].patientMainDicomTags?.patientName
            cell.lblPatientId.text = thyroidStudy[indexPath.row].patientMainDicomTags?.patientID
            cell.lblAccessionNo.text = thyroidStudy[indexPath.row].mainDicomTags?.accessionNumber
            cell.lblStudyDate.text = thyroidStudy[indexPath.row].mainDicomTags?.studyDate
            cell.lblModility.text = thyroidStudy[indexPath.row].type
            cell.lblDiscription.text = thyroidStudy[indexPath.row].mainDicomTags?.studyDescription
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView == studtTableView {
            if let viewController = AppSingleton.shared.navigateView(viewRef: .CancerViewController, storyboard: .Main) as? CancerViewController {
                viewController.modalPresentationStyle = .fullScreen
                // self.navigationController?.pushViewController(viewController, animated: true)
                AppSingleton.shared.selectedStudy = studies[indexPath.row]
                viewController.study = studies[indexPath.row]
                self.present(viewController, animated: true, completion: nil)
            }
        } else {
            if let viewController = AppSingleton.shared.navigateView(viewRef: .ThyroidViewController, storyboard: .Main) as? ThyroidViewController {
               // self.navigationController?.pushViewController(viewController, animated: true)
                viewController.modalPresentationStyle = .fullScreen
                self.present(viewController, animated: true, completion: nil)
            }
        }
    }
}
