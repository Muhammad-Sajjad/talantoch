//
//  CancerViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 06.02.22.
//

import UIKit
import SkyFloatingLabelTextField

class CancerViewController: UIViewController {
    
    @IBOutlet weak var f1button: UIButton!
    @IBOutlet weak var f2button: UIButton!
    @IBOutlet weak var f3button: UIButton!
    @IBOutlet weak var f4button: UIButton!
    @IBOutlet weak var f5button: UIButton!
    @IBOutlet weak var f6button: UIButton!
    @IBOutlet weak var calcificationTF: SkyFloatingLabelTextField!
    @IBOutlet weak var posteriorTf: SkyFloatingLabelTextField!
    @IBOutlet weak var echoTF: SkyFloatingLabelTextField!
    @IBOutlet weak var marginTF: SkyFloatingLabelTextField!
    @IBOutlet weak var orintationTF: SkyFloatingLabelTextField!
    @IBOutlet weak var shapeTF: SkyFloatingLabelTextField!
    @IBOutlet weak var lblTitle: UILabel!
    
    let diagrm: DiagramView? = nil
    let dropDown = DropDown()
    var shapeDropdwonItems:[String]{
        return ["Ovel","Round", "Irregular"]
    }
    var oriantationDropdwonItems:[String]{
        return ["Parrllel","Not Parrllel"]
    }
    
    var marginDropdwonItems:[String]{
        return ["Circumscribed", " Not Circumscribed", "Indistinct", "Angular", "Mircrolobulated", "Spiculated"]
    }
    
    var echoDropdwonItems:[String] {
        return ["Anechoic", "Hyperechoic", "Complex cystic and solid", "Hypoechoic", "Isoechoic", "Hetrogrneous"]
    }
    
    var posteriorDropdwonItems:[String]{
        return ["No posterior feaatuers","Enhancement", "Shadowing", "Combined pattern"]
    }
    
    var calcifationDropdwonItems:[String]{
        return ["Calcifations in a mass","Calcifations outside of the mass", "Intraductal calcifations"]
    }
    
    var study: Study?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        lblTitle.text = AppSingleton.shared.selectedStudy?.mainDicomTags?.studyDescription
    }
    
    
    // MARK: - Navigation
    
    @IBAction func shapeEditing(_ sender: SkyFloatingLabelTextField) {
        if sender.tag == 0 {
            configure(list: shapeDropdwonItems, tag: sender.tag)
        } else if sender.tag == 1{
            configure(list: oriantationDropdwonItems, tag: sender.tag)
        } else if sender.tag == 2{
            configure(list: marginDropdwonItems, tag: sender.tag)
        } else if sender.tag == 3{
            configure(list: echoDropdwonItems, tag: sender.tag)
        } else if sender.tag == 4{
            configure(list: posteriorDropdwonItems, tag: sender.tag)
        } else if sender.tag == 5{
            configure(list: calcifationDropdwonItems, tag: sender.tag)
        }
        dropDown.show()
    }
    
    // MARK: - Custom Function
    @IBAction func buttonPressed(_ sender: UIButton) {
        updateButtons(sender: sender)
        NotificationCenter.default.post(name: .findingChanged, object: nil, userInfo: ["tag":sender.tag])
    }

    @IBAction func saveCancerBtnTapped(_ sender: UIButton) {
        
    }
    
    @IBAction func studiesBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func scanBtnTapped(_ sender: UIButton) {
        if let viewController = AppSingleton.shared.navigateView(viewRef: .SettingViewController, storyboard: .Main) as? SettingViewController {
            viewController.modalPresentationStyle = .fullScreen
            // self.navigationController?.pushViewController(viewController, animated: true)
            self.present(viewController, animated: true, completion: nil)
        }
    }
    
    func configure(list: [String], tag: Int) {
        // dropDown.anchorView = dropdownView
        dropDown.dataSource = list
        dropDown.direction  = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:100)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            if tag == 0 {
                shapeTF.text = item
            } else if tag == 1{
                orintationTF.text = item
            } else if tag == 2{
                marginTF.text = item
            } else if tag == 3{
                echoTF.text = item
            } else if tag == 4{
                posteriorTf.text = item
            } else if tag == 5{
                calcificationTF.text = item
            }
        }
    }
    
    private func updateColors(_ buttons: [UIButton]){
        buttons.forEach{
            if $0.isSelected  {
                let alpha:CGFloat =  1.00
                let titleColor = $0.titleColor(for: .normal)?.withAlphaComponent(alpha)
                let backgroundColor =   $0.backgroundColor?.withAlphaComponent(alpha)
                $0.titleLabel?.textColor = titleColor
                $0.tintColor      = backgroundColor
                $0.backgroundColor = backgroundColor
            }else {
                let alpha:CGFloat =  0.20;
                let titleColor = $0.titleColor(for: .normal)?.withAlphaComponent(alpha)
                let backgroundColor =   $0.backgroundColor?.withAlphaComponent(alpha)
                $0.titleLabel?.textColor = titleColor
                $0.tintColor      = backgroundColor
                $0.backgroundColor = backgroundColor
            }
          
        }
        
    }
     private func updateButtons(sender: UIButton){
        let buttons:[UIButton] = [f1button,f2button,f3button,f4button,f5button,f6button]
        buttons.forEach{
            if ($0 == sender){
                $0.isSelected = true
            }else{
                $0.isSelected = false
            }
        }
         updateColors(buttons)
    }
}
