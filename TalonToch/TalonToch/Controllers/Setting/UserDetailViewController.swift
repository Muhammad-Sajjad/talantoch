//
//  UserViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 21.02.22.
//

import UIKit
import Photos
import MobileCoreServices

class UserDetailViewController: UIViewController {

    @IBOutlet weak var signatureImageView: UIImageView!
    @IBOutlet weak var phoneNoTF: UITextField!
    @IBOutlet weak var npiTF: UITextField!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var addressTF: UITextField!
    
    @IBOutlet weak var lblBtnText: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    private var actualText: String = ""
    private var maskedText: String = ""
        
    
        private var masked: Bool = true {
            didSet {
                refreshTextField()
            }
        }
    let picker = UIImagePickerController()
    var user = User()
    var npiNo = ""
    var isEdited = true
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        picker.delegate = self
        npiTF.delegate = self
        updateView()
        emailTF.isEnabled = false
        emailTF.text = user.email
        let usersData = RealmManager.shared.realm.objects(UserData.self)
        for data in usersData.enumerated() {
            if data.element.id == user.id {
                npiNo = data.element.npiNo ?? ""
            }
        }
        
    }
    
    
        
        @IBAction private func maskedTogglePressed(_ sender: Any) {
            masked = !masked
        }
        
        private func refreshTextField() {
            npiTF?.text = masked ? maskedText : actualText
        }
    // MARK: - Navigation

    func updateView() {
        if isEdited {
            nameTF.isEnabled = false
            titleTF.isEnabled = false
            npiTF.isEnabled = false
            phoneNoTF.isEnabled = false
            emailTF.isEnabled = false
            addressTF.isEnabled = false
        }
        let usersData = RealmManager.shared.realm.objects(UserData.self)
        emailTF.isEnabled = false
        for data in usersData.enumerated() {
            if data.element.id == user.id {
               // emailTF.text = data.element.email
                nameTF.text = data.element.name
                titleTF.text = data.element.title
                npiTF.text = data.element.npiNo
                phoneNoTF.text = data.element.phoneNo
                addressTF.text = data.element.address
                signatureImageView.setImage(data.element.image!)
            }
        }
    }
    @IBAction func homeBtnTapped(_ sender: UIButton) {
        if let viewController = AppSingleton.shared.navigateView(viewRef: .StudyViewController, storyboard: .Main) as? StudyViewController {
            viewController.modalPresentationStyle = .fullScreen
            AppSingleton.shared.setRoot(viewController)
        }

    }
    
    @IBAction func backBtnTapped(_ sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func uploadBtnTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: "Please choose image", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "select from photos", style: .default, handler: {(_) in
            self.chooseImage()
        }))
        alert.addAction(UIAlertAction(title: "capture from camera", style: .default, handler: {(_) in
            self.captureImage()
        }))
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = sender
            popoverPresentationController.sourceRect = sender.bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func chooseImage() {
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.mediaTypes = [kUTTypeImage] as [String]
        present(picker, animated: true, completion: nil)
    }
    
    func noCamera() {
        AlertHandler.show(with: "camera not available", self)
    }
    
    // MARK: - Capture Image from camera if available
    func captureImage() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker, animated: true, completion: nil)
        } else {
            noCamera()
        }
    }
    func setTitle() {
        print("isEdited is \(isEdited)")
        if isEdited {
            lblBtnText.text = "Save"
        } else{
            lblBtnText.text = "Edit"
        }
        isEdited.toggle()
    }
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        if npiNo == "" {
        setTitle()
        if isEdited {
            nameTF.isEnabled = false
            titleTF.isEnabled = false
            npiTF.isEnabled = false
            phoneNoTF.isEnabled = false
            emailTF.isEnabled = false
            addressTF.isEnabled = false
            let userData = UserData()
            userData.name = nameTF.text
            userData.title = titleTF.text
            userData.npiNo = npiTF.text
            userData.phoneNo = phoneNoTF.text
            userData.email = emailTF.text
            userData.address = addressTF.text
            userData.id = user.id
            if let image = signatureImageView.image!.toBase64() {
                userData.image = image
            }
            if userData.name != "" && userData.title != "" && userData.npiNo != "" && userData.phoneNo != "" && userData.email != "" && userData.address != "" && (userData.image != nil) {
            do {
                try RealmManager.shared.realm.safeWrite {
                    RealmManager.shared.realm.add(userData,update: .all)
                    updateView()
                }
            } catch {
                print("Error saving name \(error)")
            }
            } else {
                AlertHandler.show(with: "Please enter all feilds", self)
            }
        } else {
            
            nameTF.isEnabled = true
            titleTF.isEnabled = true
            npiTF.isEnabled = true
            phoneNoTF.isEnabled = true
            // emailTF.isEnabled = true
            addressTF.isEnabled = true
            
        }
        } else {
            AlertHandler.show(with: "Data aleady entered", self)
        }
        
    }
    
}


extension UserDetailViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Image picker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }
        // Show user image
        signatureImageView.image = selectedImage
        signatureImageView.contentMode = .scaleAspectFill
        
        // Dimisss
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}

extension UserDetailViewController: UITextFieldDelegate {
    func textFieldShouldEndEditing(_ textField: UITextField) -> Bool {
        return true
    }
    
    private func textFieldShouldReturn(textField: UITextField) -> Bool {
            textField.resignFirstResponder()
            return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
      
        
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == npiTF {
            textField.text = ""
        }
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            var newString = (actualText as NSString).replacingCharacters(in: range, with: string) // Apply new text
            
            // Remove all whitespaces
            newString = newString.replacingOccurrences(of: " ", with: "")
            // Remove all that is not a number
            newString = newString.filter("0123456789".contains)
            
            // Split string into components of max 4
            var components: [String] = {
                var toDeplete = newString
                var components: [String] = []
                while !toDeplete.isEmpty {
                    let length = min(toDeplete.count, 3)
                    components.append(String(toDeplete.prefix(length)))
                    toDeplete.removeFirst(length)
                }
                return components
            }()
            // Limit to maximum of 3 components
            if components.count > 3 {
                components = Array(components[0..<3])
            }
            
            // Generate masked components, replacing all characters with X
            let maskedComponents: [String] = components.map { String($0.map { character in return character }) }
            
            // Add spaces
            newString = components.joined(separator: " ")
            let maskedText = maskedComponents.joined(separator: " ")
            
            // Assign new strings
            self.actualText = newString
            self.maskedText = maskedText
            
            // Refresh field
            refreshTextField()
            
            // Disallow text field to apply it's change
            return false
        }
}
