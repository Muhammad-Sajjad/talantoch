//
//  AnaliticsViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 21.02.22.
//

import UIKit
import RealmSwift

class UsersListViewController: UIViewController {
    
    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var userTypeTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTf: UITextField!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var dropdownView: UIView!
    
    @IBOutlet weak var newUserBtn: UIButton!
    
    let dropDown = DropDown()
    var users : Results<User>?
    var dropdwonItems:[String]{
        return ["User","Adim"]
    }
    var isupdate = false
    var selectedIndex = -1
    var type = -1
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        tableView.confirmDelegate(delegate: self, dataSource: self)
        tableView.register(UserTableViewCell.identifier)
        userView.isHidden = true
        configure()
        loadItems()
        type = UserDefaults.standard.integer(forKey: "userType")
        if type == 2 {
            newUserBtn.isEnabled = false
        }
    }
    
    // MARK: - Navigation
    
    func configure(){
        
        dropDown.anchorView = dropdownView
        dropDown.dataSource = dropdwonItems
        dropDown.direction  = .bottom
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            self.userTypeTF.text = item
        }
        
        
    }
    
    func loadItems() {
        users = RealmManager.shared.realm.objects(User.self)
        tableView.reloadData()
    }
    
    func updateUser() {
        if let user = users?[selectedIndex] {
            do {
                try RealmManager.shared.realm.safeWrite {
                    user.email = emailTf.text
                    user.password = passwordTF.text
                    if userTypeTF.text == "User" {
                        user.type = 2
                    } else {
                        user.type = 1
                    }
                    isupdate = false
                    self.loadItems()
                }
            } catch {
                print("Error saving updated data \(error)")
            }
        }
    }
    
    @IBAction func typeUserStartEditing(_ sender: UITextField) {
        dropDown.show()
    }
    
    @IBAction func crossBtnTspped(_ sender: Any) {
        userView.isHidden = true
    }
    @IBAction func saveBtnTapped(_ sender: UIButton) {
        let user = User()
        user.email = emailTf.text
        user.password = passwordTF.text
        if userTypeTF.text == "User" {
            user.type = 2
        } else {
            user.type = 1
        }
        
        if isupdate {
            updateUser()
        } else {
            do {
                try RealmManager.shared.realm.safeWrite {
                    RealmManager.shared.realm.add(user)
                    emailTf.text = ""
                    passwordTF.text = ""
                    userTypeTF.text = ""
                    loadItems()
                }
            } catch {
                print("Error saving name \(error)")
            }
        }
        userView.isHidden = true
    }
    
    @IBAction func newUserTapped(_ sender: UIButton) {
        userView.isHidden = false
        
    }
}

// MARK: - View Extensions
extension UsersListViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return users?.count ?? 0
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: UserTableViewCell.identifier, for: indexPath) as! UserTableViewCell
        
        cell.lblEmail.text = users?[indexPath.row].email
        if type == 2 {
            cell.deleteBtn.isHidden = true
            cell.editBtn.isHidden = true
        }
        cell.watchForClickHandler { [self] tag in
            if tag == 0 {
                if let user = users?[indexPath.row] {
                    do {
                        try RealmManager.shared.realm.safeWrite {
                            RealmManager.shared.realm.delete(user)
                            loadItems()
                        }
                    } catch {
                        print("error deleting items \(error)")
                    }
                }
            } else if tag == 1 {
                if let viewController = AppSingleton.shared.navigateView(viewRef: .UserDetailViewController, storyboard: .Main) as? UserDetailViewController {
                    viewController.modalPresentationStyle = .fullScreen
                    viewController.user = users?[indexPath.row] ?? User()
                    self.present(viewController, animated: true, completion: nil)
                }
//                isupdate = true
//                selectedIndex = indexPath.row
//                userView.isHidden  = false
//                if let user = users?[selectedIndex] {
//                    emailTf.text = user.email
//                    passwordTF.text = user.password
//                    if user.type == 1 {
//                        userTypeTF.text = "User"
//                    } else {
//                        userTypeTF.text = "Admin"
//                    }
//                }
            }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}
