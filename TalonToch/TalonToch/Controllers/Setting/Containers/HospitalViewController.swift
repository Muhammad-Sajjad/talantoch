//
//  HospitalViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 21.02.22.
//

import UIKit
import Photos
import MobileCoreServices

class HospitalViewController: UIViewController {
    
    @IBOutlet weak var logoImage: UIImageView!
    
    @IBOutlet weak var lblAdminchange: UILabel!
    @IBOutlet weak var phoneNoTF: UITextField!
    @IBOutlet weak var hospitalNameTF: UITextField!
    @IBOutlet weak var address1TF: UITextField!
    @IBOutlet weak var countoryTF: UITextField!
    @IBOutlet weak var zipCodeTF: UITextField!
    @IBOutlet weak var stateTF: UITextField!
    @IBOutlet weak var cityTF: UITextField!
    @IBOutlet weak var address2TF: UITextField!
    
    @IBOutlet weak var saveBtn: UIButton!
    let picker = UIImagePickerController()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        picker.delegate = self
        lblAdminchange.isHidden = true
        let type = UserDefaults.standard.integer(forKey: "userType")
        if type == 2 {
            lblAdminchange.isHidden = true
            phoneNoTF.isEnabled = false
            hospitalNameTF.isEnabled = false
            address1TF.isEnabled = false
            countoryTF.isEnabled = false
            zipCodeTF.isEnabled = false
            stateTF.isEnabled = false
            cityTF.isEnabled = false
            address2TF.isEnabled = false
            saveBtn.isHidden = true
        }
        updateView()
    }
    
    
    // MARK: - Navigation
    func updateView() {
        let hospitalData = RealmManager.shared.realm.objects(HospitalData.self)
        for data in hospitalData.enumerated() {
            hospitalNameTF.text = data.element.hospitalName
            phoneNoTF.text = data.element.phoneNo
            address1TF.text = data.element.address1
            address2TF.text = data.element.address2
            cityTF.text = data.element.city
            stateTF.text = data.element.state
            zipCodeTF.text = data.element.zipCode
            countoryTF.text = data.element.country
            logoImage.setImage(data.element.logo!)
        }
    }
    
    
    @IBAction func saveBtnTapped(_ sender: Any) {
        RealmManager.shared.deleteHospitalData()
        let data = HospitalData()
        data.hospitalName = hospitalNameTF.text
        data.phoneNo = phoneNoTF.text
        data.address1 = address1TF.text
        data.address2 = address2TF.text
        data.city = cityTF.text
        data.state = stateTF.text
        data.zipCode = zipCodeTF.text
        data.country = countoryTF.text
        data.id = AppSingleton.shared.currentUserId ?? ""
        data.logo = convertImageToBase64String(img: logoImage.image ?? UIImage())
        if data.hospitalName != "" && data.phoneNo != "" && data.address1 != "" && data.address2 != "" && data.city != "" && data.state != "" && data.zipCode != "" && data.country != "" && (data.logo != nil) {
            do {
                try RealmManager.shared.realm.safeWrite {
                    RealmManager.shared.realm.add(data, update: .all)
                    updateView()
                }
            } catch {
                print("Error saving name \(error)")
            }
        } else {
            AlertHandler.show(with: "Please enter all feilds", self)
        }
    }
    
    @IBAction func uploadBtnTapped(_ sender: UIButton) {
        let alert = UIAlertController(title: "Choose Image", message: "Please choose image", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "select from photos", style: .default, handler: {(_) in
            self.chooseImage()
        }))
        alert.addAction(UIAlertAction(title: "capture from camera", style: .default, handler: {(_) in
            self.captureImage()
        }))
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = sender
            popoverPresentationController.sourceRect = sender.bounds
        }
        self.present(alert, animated: true, completion: nil)
    }
    
    func chooseImage() {
        picker.sourceType = .photoLibrary
        picker.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
        picker.mediaTypes = [kUTTypeImage] as [String]
        present(picker, animated: true, completion: nil)
    }
    
    func noCamera() {
        AlertHandler.show(with: "camera not available", self)
    }
    
    // MARK: - Capture Image from camera if available
    func captureImage() {
        if UIImagePickerController.isSourceTypeAvailable(.camera) {
            picker.sourceType = UIImagePickerController.SourceType.camera
            picker.cameraCaptureMode = .photo
            picker.modalPresentationStyle = .fullScreen
            present(picker, animated: true, completion: nil)
        } else {
            noCamera()
        }
    }
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 0.01)?.base64EncodedString() ?? ""
    }
}

extension HospitalViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    // MARK: - Image picker Delegates
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        if let editedImage = info[.editedImage] as? UIImage {
            selectedImage = editedImage
        } else if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }
        // Show user image
        logoImage.image = selectedImage
        logoImage.contentMode = .scaleAspectFill
        
        // Dimisss
        picker.dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        dismiss(animated: true, completion: nil)
    }
}
