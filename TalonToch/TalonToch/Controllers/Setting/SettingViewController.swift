//
//  SettingViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 09.02.22.
//

import UIKit
struct Settings{
    let title:String!
    let icon: UIImage!
}

class SettingViewController: UIViewController {
    
    @IBOutlet weak var menuTVwidth: NSLayoutConstraint!
    @IBOutlet weak var menuTableView: UITableView!
    @IBOutlet weak var analytViewContainer: UIView!
    @IBOutlet weak var EndPointViewContainer: UIView!
    @IBOutlet weak var userViewContainer: UIView!
    @IBOutlet weak var hospitalVContainer: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    
    var cellData = [Settings(title: "Hospital Location", icon: .menuIcons.location),
//                    Settings(title: "User Detail", icon: .menuIcons.userSetting),
                    Settings(title: "End Point Settings", icon: .menuIcons.setting),
                    Settings(title: "Users", icon: .menuIcons.userSetting)]
    
    var cellDate:[HospitalLocation] {
        return HospitalLocation.options
    }
    var isMenuOpen = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        menuTableView.confirmDelegate(delegate: self, dataSource: self)
        menuTableView.register(MenuTableViewCell.identifier)
        hospitalVContainer.isHidden = false
        analytViewContainer.isHidden = true
        EndPointViewContainer.isHidden = true
        userViewContainer.isHidden = true
        // Do any additional setup after loading the view.
        
        let type = UserDefaults.standard.integer(forKey: "userType")
        if type == 1 {
            cellData.append(Settings(title: "Text Templates", icon: .menuIcons.textTemplates))
            cellData.append(Settings(title: "About", icon: .menuIcons.about))
        }
    }
    
    
    // MARK: - Action
    
    func setView(index: Int) {
        if index == 0 {
            lblTitle.text = "Hospital Setting"
            hospitalVContainer.isHidden = false
            analytViewContainer.isHidden = true
            EndPointViewContainer.isHidden = true
            userViewContainer.isHidden = true
            
        } else if index == 1 {
            lblTitle.text = "User Setting"
            hospitalVContainer.isHidden = true
            analytViewContainer.isHidden = true
            EndPointViewContainer.isHidden = true
            userViewContainer.isHidden = false
        } else if index == 2 {
            lblTitle.text = "Analytics Setting"
            hospitalVContainer.isHidden = true
            analytViewContainer.isHidden = false
            EndPointViewContainer.isHidden = true
            userViewContainer.isHidden = true
            
        } else if index == 3 {
            lblTitle.text = "End Point Setting"
            hospitalVContainer.isHidden = true
            analytViewContainer.isHidden = true
            EndPointViewContainer.isHidden = false
            userViewContainer.isHidden = true
        }
    }
    @IBAction func logoutBtnTapped(_ sender: UIButton) {
        if let viewController = AppSingleton.shared.navigateView(viewRef: .LoginViewController, storyboard: .Main) as? LoginViewController {
            viewController.modalPresentationStyle = .fullScreen
            AppSingleton.shared.setRoot(viewController)
        }
    }
    
    @IBAction func homeBtnTapped(_ sender: UIButton) {
        if let viewController = AppSingleton.shared.navigateView(viewRef: .StudyViewController, storyboard: .Main) as? StudyViewController {
            viewController.modalPresentationStyle = .fullScreen
            AppSingleton.shared.setRoot(viewController)
        }
    }
    
    @IBAction func menuBtnTapped(_ sender: UIButton) {
        isMenuOpen.toggle()
        if isMenuOpen {
            setMenuWidth(width: 0)
        } else {
            setMenuWidth(width: 300)
        }
    }
    
    func setMenuWidth(width: CGFloat) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) { [self] in
            self.menuTVwidth.constant = width
            UIView.animate(withDuration: 0.2) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
}


extension SettingViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cellData.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if tableView == menuTableView {
            let cell = tableView.dequeueReusableCell(withIdentifier: MenuTableViewCell.identifier, for: indexPath) as! MenuTableViewCell
            var content = cell.defaultContentConfiguration()
            
            // Configure content.
            let setting = cellData[indexPath.row]
            content.image = setting.icon
            content.imageProperties.tintColor = .lightGray
            content.imageProperties.preferredSymbolConfiguration = .init(pointSize: 10)
            content.text = setting.title
            // content.textProperties.font = FontBook.Regular.of(size: 20)
            cell.contentConfiguration = content
            return cell
        }
        
        return UITableViewCell()
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        setView(index: indexPath.row)
    }
    
}

struct HospitalLocation{
    let title:String!
    let isShowSwitch:Bool!
}


extension HospitalLocation{
    static  var options:[HospitalLocation]{
        return [
            HospitalLocation(title:"Hospital Name",isShowSwitch: false),
            HospitalLocation(title:"Logo",isShowSwitch: false),
            HospitalLocation(title:"Address",isShowSwitch: false),
            HospitalLocation(title: "Dark Mode",isShowSwitch:true),
            HospitalLocation(title:"Enable Text Templets",isShowSwitch: true),
            
        ]
        
    }
}
