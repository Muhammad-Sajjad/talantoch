//
//  DiagramView.swift
//  TalonTouch
//
//  Created by syed zia on 29/10/2021.
//
import SkyFloatingLabelTextField
import UIKit

class DiagramView: UIView {

    // MARK:- IBOITLETS -
    @IBOutlet var view: UIView!
    @IBOutlet weak var patientNameField: SkyFloatingLabelTextField!
    @IBOutlet weak var assessionNumberField : SkyFloatingLabelTextField!
    @IBOutlet weak var positionField: SkyFloatingLabelTextField!
    @IBOutlet weak var cMFNField : SkyFloatingLabelTextField!
    @IBOutlet weak var periarecalarField: SkyFloatingLabelTextField!
    @IBOutlet weak var sizeField : SkyFloatingLabelTextField!
    @IBOutlet weak var daigramImageView: UIImageView!
    @IBOutlet weak var f1button: UIButton!
    @IBOutlet weak var f2button: UIButton!
    @IBOutlet weak var f3button: UIButton!
    @IBOutlet weak var f4button: UIButton!
    @IBOutlet weak var f5button: UIButton!
    @IBOutlet weak var f6button: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        NotificationCenter.default.addObserver(self, selector: #selector(updateFindings(_:)), name: .findingChanged,object: nil)
        //NotificationCenter.default.addObserver(self, selector: #selector(updateProjects), name: .projectUpdate, object: nil)
        // Initialization code
        patientNameField.text = AppSingleton.shared.selectedStudy?.patientMainDicomTags?.patientName
        assessionNumberField.text = AppSingleton.shared.selectedStudy?.mainDicomTags?.accessionNumber
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        layer.cornerRadius = 20
        layer.masksToBounds = true
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        view.backgroundColor = .clear
        loadNib()
    }

     // calling of loadNib() method is not required here.
    // this initializer will call init(frame: CGRect) any how we are calling loadnib in that method.
    //for demo purpose i added in every places.
    init (labelText: String) {
        super.init(frame: .zero)
        let view = loadNib()
        view.backgroundColor = .clear
    }

    // If you embed the view in parent view, this method will call
    //here we are added class name to file owner so called load() method here.
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        loadNib()
    }

    @discardableResult func loadNib() -> UIView {
        let view = Bundle.main.loadNibNamed("DiagramView", owner: self, options: nil)?.first as?  UIView
        view?.autoresizingMask = [.flexibleHeight, .flexibleWidth]
        view?.frame = bounds
        addSubview(view!)
        return view!
    }
    
    
    
    @IBAction func editButtonPressed(_ sender: UIButton) {
        
        
    }
    
}

// MARK:- Private Funcs -
 extension DiagramView {
    private func updateColors(_ buttons: [UIButton]){
        buttons.forEach{
            if $0.isSelected  {
                let alpha:CGFloat =  1.00
                let tintColor =  $0.tintColor?.withAlphaComponent(alpha)
                $0.tintColor        = tintColor
        
            }else {
                let alpha:CGFloat =  0.25
                let tintColor =  $0.tintColor?.withAlphaComponent(alpha)
                $0.tintColor        = tintColor
            }
          
        }
        
    }
     private func updateButtons(tag: Int){
        let buttons:[UIButton] = [f1button,f2button,f3button,f4button,f5button,f6button]
        buttons.forEach{
            if ($0.tag == tag){
                $0.isSelected = true
            }else{
                $0.isSelected = false
            }
        }
         updateColors(buttons)
    }
     func setFirstButtonSelected(){
         
         updateButtons(tag: 0)
     }
     
     @objc  private func updateFindings(_ notification: Notification) {
         let userInfo = notification.userInfo as! [String: Any];
         let tag  = userInfo["tag"] as! Int
         updateButtons(tag: tag)
          
       }
}
