//
//  LoginViewController.swift
//  TalonToch
//
//  Created by Sajjad Malik on 01.02.22.
//

import UIKit
import RealmSwift

class LoginViewController: UIViewController {
    
    @IBOutlet weak var keepMeLoginBtn: UIButton!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var scurePassImage: UIImageView!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    
    var toggle:Bool = false
    var isScure:Bool = false
    // APP Delegate
    var users = [User]()
    
    weak var appDelegate: AppDelegate!
    // let realm = try! Realm()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        infoView.isHidden = true
        appDelegate = UIApplication.shared.delegate as? AppDelegate
        print(RealmManager.shared.realm.configuration.fileURL)
//        emailTF.text = "user@mail.com"
//        passwordTF.text = "123456"
        // fetchData()
        let data = UserDefaults.standard.bool(forKey: "userAdded")
        let isUser = RealmManager.shared.realm.objects(User.self)
        if !data && isUser.count < 1 {
            load()
        }
    }
    
   
    
    func load() {
        if let fileLocation = Bundle.main.url(forResource: "users", withExtension: "json") {
            // do catch in case of an error
            do {
                let data = try Data(contentsOf: fileLocation)
                let jsonDecoder = JSONDecoder()
                let dataFromJson = try jsonDecoder.decode([User].self, from: data)
                
                self.users = dataFromJson
                addUser()
            } catch {
                print(error)
            }
        }
    }
    
    func authUser(completion: @escaping (Bool) -> Void) {
        let data = RealmManager.shared.realm.objects(User.self)
        let userData = data.toArray(ofType: User.self)
        for user in userData {
            if user.email == emailTF.text && user.password == passwordTF.text {
                AppSingleton.shared.currentUserId = user.id
                completion(true)
                AlertHandler.show(with: "login succcessfuly.", self)
                UserDefaults.standard.set(user.type, forKey: "userType")
                if let viewController = AppSingleton.shared.navigateView(viewRef: .StudyViewController, storyboard: .Main) as? StudyViewController {
                    viewController.modalPresentationStyle = .fullScreen
                    AppSingleton.shared.setRoot(viewController)
                }
            } else {
                completion(false)
            }
        }
    }
    
    func addUser() {
        do {
            RealmManager.shared.deleteUsers()
            for object in self.users {
                try RealmManager.shared.realm.safeWrite {
                    RealmManager.shared.realm.add(object)
                    UserDefaults.standard.set(true, forKey: "userAdded")
                }
            }
        } catch let error {
            print(error.localizedDescription)
        }
    }
    
    
    
    @IBAction func keepMeLoginTapped(_ sender: UIButton) {
        //  keepMeLoginBtn.setImage("ic_radio_selected", for: .normal)
        toggle.toggle()
        if toggle {
            keepMeLoginBtn.setImage(UIImage(named: "ic_radio_selected"), for: .normal)
        } else {
            keepMeLoginBtn.setImage(UIImage(named: "ic_radio_unselected"), for: .normal)
        }
    }
    
    @IBAction func moreInfoTapped(_ sender: UIButton) {
        infoView.isHidden = false
    }
    
    @IBAction func acceptBtnTapped(_ sender: UIButton) {
        infoView.isHidden = true
    }
    
    @IBAction func seePasswordTapped(_ sender: UIButton) {
        isScure.toggle()
        if isScure {
            passwordTF.isSecureTextEntry = false
            scurePassImage.image = UIImage(named: "visibility_on")
        } else {
            passwordTF.isSecureTextEntry = true
            scurePassImage.image = UIImage(named: "visibility_off")
        }
    }
    
    @IBAction func loginBtnTapped(_ sender: UIButton) {
        guard let mail = emailTF.text?.isEmail() else { return  }
        let password = passwordTF.text
        if  mail && password != "" {
            authUser { success in
                if success {
                    if let viewController = AppSingleton.shared.navigateView(viewRef: .StudyViewController, storyboard: .Main) as? StudyViewController {
                        viewController.modalPresentationStyle = .fullScreen
                        //self.present(viewController, animated: true, completion: nil)
                        AppSingleton.shared.setRoot(viewController)
                    }
                    
                } else {
                    AlertHandler.show(with: "Please Enter Valid Email and Password.", self)
                }
            }
        } else {
            AlertHandler.show(with: "Please Enter Valid Email and Password.", self)
        }
    }
}

