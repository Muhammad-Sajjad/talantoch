//
//  AlertHandler.swift
//  TalonToch
//
//  Created by Sajjad Malik on 12.02.22.
//

import Foundation
import PopupDialog

typealias AlertCompletion = () -> Void
class AlertHandler {
    
    // Show Alert
    class func show(with message: String, _ view: UIViewController) {
        let popup = PopupDialog(title: "TalonToch", message: message)
        let cancelButton = CancelButton(title: "Ok", action: nil)
        popup.addButton(cancelButton)
        view.present(popup, animated: true, completion: nil)
    }
    
    class func show(message: String, view: UIViewController, completion: @escaping AlertCompletion, cBtnTitle: String) {
        let popup = PopupDialog(title: "TalonToch", message: message)
        let cancelButton = CancelButton(title: cBtnTitle, action: {
            completion()
        })
        popup.addButton(cancelButton)
        view.present(popup, animated: true, completion: nil)
    }
    
    class func show(message: String, view: UIViewController, completion: @escaping AlertCompletion, cBtnTitle: String, secBtnTitle: String) {
        let popup = PopupDialog(title: "TalonToch", message: message)
        let cancelButton = CancelButton(title: cBtnTitle, action: nil)
        let secBtn = DefaultButton(title: secBtnTitle, action: {
            completion()
        })
        popup.addButtons([cancelButton, secBtn])
        view.present(popup, animated: true, completion: nil)
    }
}
