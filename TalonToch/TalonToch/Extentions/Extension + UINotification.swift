//
//  Extension + UINotification.swift
//  TalonToch
//
//  Created by Sajjad Malik on 12.02.22.
//

import Foundation
extension Notification.Name {
    static let findingChanged = Notification.Name(rawValue: "findingChanged")
}
