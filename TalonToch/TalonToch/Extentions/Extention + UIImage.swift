//
//  Extention + UIImage.swift
//  TalonToch
//
//  Created by Sajjad Malik on 09.02.22.
//

import Foundation
import UIKit

extension UIImageView {
    func setImage(_ image: UIImage?, animated: Bool = true) {
        let duration = animated ? 0.3 : 0.0
        UIView.transition(with: self, duration: duration, options: .transitionCrossDissolve, animations: {
            self.image = image
        }, completion: nil)
    }
    
    func ofColor(_ color: UIColor) {
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
    func setImage(_ url: String, isUser: Bool = false) {
        if let data = Data(base64Encoded: url, options: .ignoreUnknownCharacters) {
            if let decodedimage = UIImage(data: data) {
                self.image = decodedimage
            }
        }
    }
}

extension UIImage {
    struct menuIcons{
        static let analytics = UIImage(named: "ic_analytics")!
        static let about = UIImage(named: "ic_menu_about")!
        static let location = UIImage(named: "ic_menu_location")!
        static let setting = UIImage(named: "ic_menu_settings")!
        static let userSetting = UIImage(named: "ic_menu_set_user")!
        static let textTemplates = UIImage(named: "ic_text")!
    }
    struct passowdIcons{
        static let selected = UIImage(named: "visibility_on")!
        static let unselected = UIImage(named: "visibility_off")!
        
    }
    
    func toBase64() -> String? {
        guard let imageData = self.jpegData(compressionQuality: 0.01) else { return nil }
        return imageData.base64EncodedString(options: Data.Base64EncodingOptions.lineLength64Characters)
    }
    
    func convertImageToBase64String (img: UIImage) -> String {
        return img.jpegData(compressionQuality: 0.01)?.base64EncodedString() ?? ""
    }
}
