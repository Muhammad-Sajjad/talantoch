//
//  Extention + UITableView.swift
//  TalonToch
//
//  Created by Sajjad Malik on 04.02.22.
//

import Foundation
import UIKit

extension UITableView {
    
    func confirmDelegate(delegate: UITableViewDelegate, dataSource: UITableViewDataSource) {
        self.delegate = delegate
        self.dataSource = dataSource
        self.separatorStyle = .none
        self.separatorColor = .clear
        self.tableFooterView = UIView(frame: .zero)
        self.backgroundColor = .clear
    }
    
    func register(_ identifier: String) {
        self.register(UINib(nibName: identifier, bundle: nil), forCellReuseIdentifier: identifier)
    }
}
