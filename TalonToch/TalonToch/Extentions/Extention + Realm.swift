//
//  Extention + Realm.swift
//  TalonToch
//
//  Created by Sajjad Malik on 11.02.22.
//

import Foundation
import RealmSwift

extension Realm {
    public func safeWrite(_ block: (() throws -> Void)) throws {
        if isInWriteTransaction {
            try block()
        } else {
            try write(block)
        }
    }
}
