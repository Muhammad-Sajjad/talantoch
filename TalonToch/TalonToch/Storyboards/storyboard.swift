//
//  storyboard.swift
//  TalonToch
//
//  Created by Sajjad Malik on 02.02.22.
//

import Foundation

enum StoryboardReference: String {
    case Main
}

// MARK: - View Controller Reference
enum ViewControllerReference: String {
    
    // MARK: - Main
    /// Main Stroyboard contain the following views
    case LoginViewController
    case StudyViewController
    case CancerViewController
    case ThyroidViewController
    case SettingViewController
    case UserDetailViewController
}

// MARK: - Navigation Reference
enum NavigationReference: String {
    
    // MARK: - Main
    /// Main Stroyboard contain the following navigation
    case Appnavigation
}
