//
//  AppSinglton.swift
//  TalonToch
//
//  Created by Sajjad Malik on 03.02.22.
//

import Foundation
import UIKit

class AppSingleton: NSObject {
    
    // MARK: - Shared instance
    static let shared = AppSingleton()
    
    var selectedStudy: Study?
    var appDelegate: AppDelegate!
    var currentUserId: String?
    override init() {
        appDelegate = UIApplication.shared.delegate as? AppDelegate
    }
    
    // MARK: - Shared methods
    func getStoryboardReference(storyboard: StoryboardReference) -> UIStoryboard {
        return UIStoryboard(name: storyboard.rawValue, bundle: nil)
    }
    
    func navigateView(viewRef: ViewControllerReference, storyboard: StoryboardReference) -> UIViewController {
        let view = getStoryboardReference(storyboard: storyboard).instantiateViewController(withIdentifier: viewRef.rawValue)
        return view
    }
    
    func setRoot(_ view: UIViewController) {
        if let scene = UIApplication.shared.connectedScenes.first {
            guard let windowScene = (scene as? UIWindowScene) else { return }
            let window: UIWindow = UIWindow(frame: windowScene.coordinateSpace.bounds)
            self.appDelegate.window = window
            window.windowScene = windowScene
            window.rootViewController = view
            window.makeKeyAndVisible()
        }
    }
    
    func getTop() -> UIViewController? {
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        if var topController = keyWindow?.rootViewController {
            while let presentedViewController = topController.presentedViewController {
                topController = presentedViewController
            }
            
            // topController should now be your topmost view controller
            return topController
        }
        
        return nil
    }
}
