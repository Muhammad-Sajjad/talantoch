//
//  Study.swift
//  TalonToch
//
//  Created by Sajjad Malik on 20.02.22.
//

import Foundation

struct Study : Codable {
    let iD : String?
    let isStable : Bool?
    let lastUpdate : String?
    let mainDicomTags : MainDicomTags?
    let parentPatient : String?
    let patientMainDicomTags : PatientMainDicomTags?
    let series : [String]?
    let type : String?

    enum CodingKeys: String, CodingKey {

        case iD = "ID"
        case isStable = "IsStable"
        case lastUpdate = "LastUpdate"
        case mainDicomTags = "MainDicomTags"
        case parentPatient = "ParentPatient"
        case patientMainDicomTags = "PatientMainDicomTags"
        case series = "Series"
        case type = "Type"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        iD = try values.decodeIfPresent(String.self, forKey: .iD)
        isStable = try values.decodeIfPresent(Bool.self, forKey: .isStable)
        lastUpdate = try values.decodeIfPresent(String.self, forKey: .lastUpdate)
        mainDicomTags = try values.decodeIfPresent(MainDicomTags.self, forKey: .mainDicomTags)
        parentPatient = try values.decodeIfPresent(String.self, forKey: .parentPatient)
        patientMainDicomTags = try values.decodeIfPresent(PatientMainDicomTags.self, forKey: .patientMainDicomTags)
        series = try values.decodeIfPresent([String].self, forKey: .series)
        type = try values.decodeIfPresent(String.self, forKey: .type)
    }

}


struct MainDicomTags : Codable {
    let accessionNumber : String?
    let referringPhysicianName : String?
    let requestedProcedureDescription : String?
    let studyDate : String?
    let studyDescription : String?
    let studyID : String?
    let studyInstanceUID : String?
    let studyTime : String?

    enum CodingKeys: String, CodingKey {

        case accessionNumber = "AccessionNumber"
        case referringPhysicianName = "ReferringPhysicianName"
        case requestedProcedureDescription = "RequestedProcedureDescription"
        case studyDate = "StudyDate"
        case studyDescription = "StudyDescription"
        case studyID = "StudyID"
        case studyInstanceUID = "StudyInstanceUID"
        case studyTime = "StudyTime"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        accessionNumber = try values.decodeIfPresent(String.self, forKey: .accessionNumber)
        referringPhysicianName = try values.decodeIfPresent(String.self, forKey: .referringPhysicianName)
        requestedProcedureDescription = try values.decodeIfPresent(String.self, forKey: .requestedProcedureDescription)
        studyDate = try values.decodeIfPresent(String.self, forKey: .studyDate)
        studyDescription = try values.decodeIfPresent(String.self, forKey: .studyDescription)
        studyID = try values.decodeIfPresent(String.self, forKey: .studyID)
        studyInstanceUID = try values.decodeIfPresent(String.self, forKey: .studyInstanceUID)
        studyTime = try values.decodeIfPresent(String.self, forKey: .studyTime)
    }

}

struct PatientMainDicomTags : Codable {
    let patientBirthDate : String?
    let patientID : String?
    let patientName : String?
    let patientSex : String?

    enum CodingKeys: String, CodingKey {

        case patientBirthDate = "PatientBirthDate"
        case patientID = "PatientID"
        case patientName = "PatientName"
        case patientSex = "PatientSex"
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        patientBirthDate = try values.decodeIfPresent(String.self, forKey: .patientBirthDate)
        patientID = try values.decodeIfPresent(String.self, forKey: .patientID)
        patientName = try values.decodeIfPresent(String.self, forKey: .patientName)
        patientSex = try values.decodeIfPresent(String.self, forKey: .patientSex)
    }

}
