//
//  User.swift
//  TalonToch
//
//  Created by Sajjad Malik on 11.02.22.
//

import Foundation
import UIKit
import RealmSwift

class User: Object, Codable {
    
    // MARK: - Data Members
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var type: Int = 0
    @objc dynamic var email: String?
    @objc dynamic var password: String?
    
    override static func primaryKey() -> String? {
            return "id"
    }
    
    // MARK: - Class init
    override init() {
        type = 0
        email = ""
        password = ""
        
    }
    init(_ user: User) {
        self.type = user.type
        self.email = user.email
        self.password = user.password
        
    }
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case type
        case email
        case password
    }
    
    // Encode data
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(type, forKey: .type)
        try container.encode(email, forKey: .email)
        try container.encode(password, forKey: .password)
    }
    
        /// Creates a new instance by decoding from the given decoder.
       /// - Parameter decoder: the decoder to read data from.
       /// - Throws: This initializer throws an error if reading from the decoder fails, or if the data read is corrupted or otherwise invalid.
    required init(from decoder: Decoder) throws {
        //  Returns the data stored in this decoder as represented in a container keyed by the given key type.
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        /// Decodes a value of the given type for the given key, if present.
        /// - throws: `DecodingError.typeMismatch` if the encountered encoded value  is not convertible to the requested type.
        self.type = try values.decode(Int.self, forKey: .type)
        self.email = try values.decode(String.self, forKey: .email)
        self.password = try values.decode(String.self, forKey: .password)
    }
}
