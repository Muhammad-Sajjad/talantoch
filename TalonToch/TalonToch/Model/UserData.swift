//
//  UserData.swift
//  TalonToch
//
//  Created by Sajjad Malik on 11.03.22.
//


import Foundation
import UIKit
import RealmSwift

class UserData: Object, Codable {
    
    // MARK: - Data Members
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var name: String?
    @objc dynamic var title: String?
    @objc dynamic var npiNo: String?
    @objc dynamic var phoneNo: String?
    @objc dynamic var email: String?
    @objc dynamic var address: String?
    @objc dynamic var image: String?
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: - Class init
    override init() {
        name = ""
        title = ""
        npiNo = ""
        phoneNo = ""
        email = ""
        address = ""
        image = ""
        
    }
    init(_ data: UserData) {
        self.name =  data.name
        self.title = data.title
        self.npiNo = data.npiNo
        self.phoneNo = data.phoneNo
        self.email = data.email
        self.address = data.address
        self.image = data.image
        
    }
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case name
        case title
        case npiNo
        case phoneNo
        case email
        case address
        case image
    }
    
    // Encode data
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(title, forKey: .title)
        try container.encode(npiNo, forKey: .npiNo)
        try container.encode(phoneNo, forKey: .phoneNo)
        try container.encode(email, forKey: .email)
        try container.encode(address, forKey: .address)
        try container.encode(address, forKey: .image)
    }
    
    /// Creates a new instance by decoding from the given decoder.
    /// - Parameter decoder: the decoder to read data from.
    /// - Throws: This initializer throws an error if reading from the decoder fails, or if the data read is corrupted or otherwise invalid.
    required init(from decoder: Decoder) throws {
        //  Returns the data stored in this decoder as represented in a container keyed by the given key type.
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        /// Decodes a value of the given type for the given key, if present.
        /// - throws: `DecodingError.typeMismatch` if the encountered encoded value  is not convertible to the requested type.
        self.name = try values.decode(String.self, forKey: .name)
        self.title = try values.decode(String.self, forKey: .title)
        self.npiNo = try values.decode(String.self, forKey: .npiNo)
        self.phoneNo = try values.decode(String.self, forKey: .phoneNo)
        self.email = try values.decode(String.self, forKey: .email)
        self.address = try values.decode(String.self, forKey: .address)
        self.image = try values.decode(String.self, forKey: .image)
    }
}
