//
//  HospitalData.swift
//  TalonToch
//
//  Created by Sajjad Malik on 12.03.22.
//


import Foundation
import UIKit
import RealmSwift

class HospitalData: Object, Codable {
    
    // MARK: - Data Members
    @objc dynamic var id = UUID().uuidString
    @objc dynamic var hospitalName: String?
    @objc dynamic var phoneNo: String?
    @objc dynamic var address1: String?
    @objc dynamic var address2: String?
    @objc dynamic var city: String?
    @objc dynamic var state: String?
    @objc dynamic var zipCode: String?
    @objc dynamic var country: String?
    @objc dynamic var logo: String?
    override static func primaryKey() -> String? {
        return "id"
    }
    
    // MARK: - Class init
    override init() {
        hospitalName = ""
        phoneNo = ""
        address1 = ""
        address2 = ""
        city = ""
        state = ""
        zipCode = ""
        country = ""
        logo = ""
    }
    init(_ data: HospitalData) {
        self.hospitalName =  data.hospitalName
        self.phoneNo =  data.phoneNo
        self.address1 =  data.address1
        self.address2 =  data.address2
        self.city =  data.city
        self.state =  data.state
        self.zipCode =  data.zipCode
        self.country =  data.country
        self.logo =  data.logo
        
    }
    
    // MARK: - CodingKeys
    enum CodingKeys: String, CodingKey {
        case hospitalName
        case phoneNo
        case address1
        case address2
        case city
        case state
        case zipCode
        case country
        case logo
    }
    
    // Encode data
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(hospitalName, forKey: .hospitalName)
        try container.encode(phoneNo, forKey: .phoneNo)
        try container.encode(address1, forKey: .address1)
        try container.encode(address2, forKey: .address2)
        try container.encode(city, forKey: .city)
        try container.encode(state, forKey: .state)
        try container.encode(zipCode, forKey: .zipCode)
        try container.encode(country, forKey: .country)
        try container.encode(logo, forKey: .logo)
    }
    
    /// Creates a new instance by decoding from the given decoder.
    /// - Parameter decoder: the decoder to read data from.
    /// - Throws: This initializer throws an error if reading from the decoder fails, or if the data read is corrupted or otherwise invalid.
    required init(from decoder: Decoder) throws {
        //  Returns the data stored in this decoder as represented in a container keyed by the given key type.
        let values = try decoder.container(keyedBy: CodingKeys.self)
        
        /// Decodes a value of the given type for the given key, if present.
        /// - throws: `DecodingError.typeMismatch` if the encountered encoded value  is not convertible to the requested type.
        self.hospitalName = try values.decode(String.self, forKey: .hospitalName)
        self.phoneNo = try values.decode(String.self, forKey: .phoneNo)
        self.address1 = try values.decode(String.self, forKey: .address1)
        self.address2 = try values.decode(String.self, forKey: .address2)
        self.city = try values.decode(String.self, forKey: .city)
        self.state = try values.decode(String.self, forKey: .state)
        self.zipCode = try values.decode(String.self, forKey: .zipCode)
        self.country = try values.decode(String.self, forKey: .country)
        self.logo = try values.decode(String.self, forKey: .logo)
    }
}
